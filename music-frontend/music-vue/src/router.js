import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import(/* webpackChunkName: "Home" */ './views/Home.vue')
        },
        {
            path: '/singer',
            name: 'Singer',
            component: () => import(/* webpackChunkName: "Singer" */ './views/Singer.vue')
        },
        {
            path: '/songs',
            name: 'Songs',
            component: () => import(/* webpackChunkName: "Song" */ './views/Songs.vue')
        },
        {
            path: '/albums',
            name: 'Albums',
            component: () => import(/* webpackChunkName: "Albums" */ './views/Albums.vue')
        },
        {
            path: '/register',
            name: 'Register',
            component: () => import(/* webpackChunkName: "Register" */ './views/Register.vue')
        },
        {
            path: '/login',
            name: 'Login',
            component: () => import(/* webpackChunkName: "Login" */ './views/Login.vue')
        }
    ]
});
