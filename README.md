# README

为了方便大家了解整个过程，把不同阶段的内容打成了不同的 `version` 来加以区分，`version` 名称以字母 `v` + `数字序号` 来命名。

### 当前 `tag`

当前这个 `version` 为 `v1`，主要包含以下几部分内容：

> 注：`<dir>` 表示是一个文件夹

- template`<dir>` : 提供应用最原始的存静态页面等资源
- music-frontend`<dir>` : 存放应用前端源码
    - music-vue`<dir>` : vue 版，当前 `version` 主要内容
    - music-react`<dir>` : 暂不存在，，后续 `version` 添加，存放 react 版源码
- music-backend`<dir>` : 暂不存在，后续 `version` 添加，项目后端源码
- project-database`<dir>` : 存放应用数据以及数据导入脚本，后续使用

### music-vue

##### 进入 `music-vue` 目录

```
cd music-frontend/music-vue
```

##### 安装依赖

如果是首次使用，进入 `music-vue` 目录以后，则需要安装依赖模块

```
npm install
```

##### 启动

```
npm run serve
```

##### 目录文件说明

当前这个 `version` 只是根据初始提供的 template 进行了模块化与组件化组织，下面就一些重点进行说明

- 路由

在 `router.js` 中定义了应用的路由

    - / => ./views/Home.vue
    - /singer => ./views/Singer.vue
    - /songs => ./views/Songs.vue
    - /albums => ./views/Albums.vue
    - /register => ./views/Register.vue
    - /login => ./views/Login.vue

所有路由对应的组件都存放在 `./src/views` 目录下，同时在页面也存在一些可复用的局部组件，比如头、尾、分页等，这些组件也称为功能组件或业务组件，它们都存放在 `./src/components` 目录下，应用中使用到静态资源都存放在了 `./src/assets` 目录下，同时也根据页面组件与业务组件进行了划分